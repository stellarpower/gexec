# gExec

A simple GRPC-based system for remotely executing a binary or script on a server and viewing as if called from the client. Can be used inside containers to bridge to the host or between each other in a simple manner, or wrap a script into a simple server.

## What it's for

gExec is a simple program for performing remote execution of a binary, script, or other executable file, across a network or Unix domain socket. Start gExec on a server, and connect to it using client mode in order to execute programs on the server, pass them input, and receive stdout, stderr, and the return status on the client machine, as if the process had been executed locally.

In a containerised system, this program can be used as an easy solution to the not-so-trivial problem of bridging between containers and the host machine, if for example, part of your deployment is tied to hardware or other resources that can't (yet) be accessed inside containers. Or across a trusted local network, it provides a simple way to allow access to a script or program without the overhead of setting up a full custom server, and without interfacing with other, lower-level tools directly. If you want a your raspberry Pi to be able to notify you when it's finished a long job by displaying a dialogue box on your desktop, or making it beep at you, then this program is designed to provide a quick and simple solution to this problem with less overhead than other methods.

gExec utilises the GRPC library internally to do this, which provides a much more elegant solution than tools such as named pipes, netcat, HTTP, more complex reverse SSH setups, which require a certian amount of manual handling of different connection scenarios. However, I didn't find any existing packages that didn't require writing a custom protocol and interfacing with GRPC directly, so I wrote this, ideally to provide an off-the-shelf solution for similar use-cases, so you don't need to implement custom server logic in a script, be that for TCP or Unix.


## How to use it
As gExec uses GRPC as its underlying communication mechanism, the client and server can communicate over any protocol that the GRPC system (and specifically, the Go library) can support. The command line is (necessarily) a little complicated as arguments also have to be passed to the process being executed. Follow the examples below reasonably carefully, before the rest is detailed at the end.


The simplest setup would involve starting the server like below. Let's use a unix socket for communication:

`user@server:~> gexec server unix /tmp/MyGExecServer.sock`

Now let's connect the client, and see what goodies Alice is hiding:

```
user@client:~> gexec client -x ls unix://tmp/MyGExecServer.sock -- -lah /home/alice
total 1.4G
drwxr-xr-x 173 alice alice  300 ago 15 18:45  .
drwxr-xr-x   9 root  root  4.0K may  7 17:04  ..
drwxrwxr-x   2 alice alice    2 nov 26  2017  Audio
-rw-rw-r--   1 alice alice 4.0M jun  8  2020  2020-06-07.jpg
....
user@client:~>
```
It returns the output as if we had called the command locally. The remote program we want to call is specified by the -x/--executable flag. Because this is optional (more below), it is an *option* to the client verb, and so must come before the *positional* argument, which specifies the connection string.

Let's find out if we're running any containers at the moment:

`user@client:~> gexec client -x /usr/local/bin/podman unix://tmp/MyGExecServer.sock -- ps`

We can pass an absolute path to -x; it uses the PATH variable to work things out. We could even try being *cheeky*:

`user@client:~> gexec client -x sudo unix://tmp/MyGExecServer.sock -- rm -R /`

Now, we see why it's potentially very dangerous to operate a server like this. If the user who stared the server has sudo priveliges without needing to enter their password, this would have been very bad on a real machine. gExec was designed to solve a problem quickly and easily, and the locked mode below improves things, but I wouldn't trust its security in production at this stage.

So, to enable locked mode, we need to provide a default executable for the server to called. Let's kill the old server and try using TCP now. We do this using the similar -x/--default-executable flag for the server:

`user@server:~> gexec server -x ls -l tcp localhost:8008`

and we locked it using the -l/--locked flag. Now, the client cannot execute any other commands without hitting an error. Supporting multiple commands would be useful, but, as an extra complication it is not implemented. If you need to do this, it is recommended instead that you use a simple script that wraps several known commands that you want to allow, and calls the appropriate one using its own arguments, and lock the server to this script instead.


At this point, it's worth mentioning that the executable is an optional switch for both client and server. The reason is to provide flexibility. If the server is locked, then it requires a default to call, as otherwise locking it has no meaning. If it is not locked however, then it can still be useful to have a sensible default program to call. The client can be freed of having to know exactly what is being executed and simply provide some arguments.

Similarly, from the client's side, if we lock the server, the client doesn't have any choice in what is allowed to be called, so it makes sense toallow the client to call the server providing only arguments to the server. But if the client does provide an executable, then we allow some checking that still enable the request to go ahead.

If "program" on the server's path refers to `/path/program`, then any combination of the below should enable the request to complete successfully:

| Client        | Server        |
|---------------|---------------|
| /path/program | /path/program |
| program       | /path/program |
| /path/program | program       |
| program       | program       |
|               |               |

This has been tested at the time of writing, but there may well be some corner cases.


Hence, if the server is unlocked, the server can specify or omit a default executable to call, and the client can call with or without a custom executable. If the server did not specify a default and the client calls without a custom executable, then the request cannot be serviced, otherwise, it goes ahead.

If the server is locked (and this is recommended), then the server must provide a defaul executable; but the client can still call with or without a custom executable, and if it does provide one, the server tries to check it flexibly to see if it can stll service the request.



In a similar vein, the server can also provide default arguents to the executable. These must be provided in the same manner as are provided to the client: after the positional arguments and a double dash:

`user@client:~> gexec client -x du localhost:8008 -- -h --summarize`

This could be used to create a microservice allowing the client to pass in the path to a folder and find out the disk space being used by it.

In a case like the below:

`user@cerver:~> gexec server unix /tmp/MyGexecServer.sock -- -lah /bin`

the server is not locked and so will append those arguments to any executbale the client requests, which is most likely a bit useless as default arguments only really make sense for a specific program or set of similar programs.

## Input/Output
- stdout and stderr from the remote process by default will be printed to the same on the client.
- To output to files instead, use the `-e` and options `-o`, passing the files where you want this to be saved.
- Input similarly can be read from a file using `-i file`, but also, can be directly read from a string using `-I 'Text to give to the remote process'`
- Currently, reading input from the client's stdin isn't supported, but would be a part of the future streaming mode.

## Modes of operation
Without any restrictions, gExec essentially allows anyone with an ability to connect to the server to execute any process on that machine as the current user. Start the server listening on an open port and you're essentially allowing anyone who can reach your machine to run a shell without authentication. For this reason, the server may be started in a "locked" mode, where the server only permits calling out to one specific file, and any client requests that do not "match" this file will be rejected. It is recommended that this feature be used wherever possible; starting in unlocked mode is opening quite a significant hole and should be reserved exclusively for very controlled environments where the convenience greatly outweighs the risks.

It is also recommended to use unix sockets where possible in favour of TCP, as permissions and ownership can be changed on the socket to restrict access in a way that cannot on network sockets. Note that currently the socket is created by gExec in server mode, and with default permissions - you would have to change ownserhip and permissions after launching the server, at this time.

Currently, gExec only implements a "oneshot" mode. The entire stdin must be provided in advance, along with the command-line arguments, and the client sends this to the server, awaits the response, outputs this, and exits with a return status to match that of the remote process. This was simpler to implement, but is less flexible in pipeline setups. Ideally, the program would be able to send packets of input, output, and error, backwards and forwards across the connection, to function transparently compared to native execution. GRPC has the concept of streaming messages, and as utilising these hsould make possible a streaming mode that would function like this. However, this is currently not implemented.

## Command Line

gExec uses the "verb" or "subcommand" format for its command line - `gexec client` or `gexec server` are the two main verbs. the format used is:

`gexec *global options* verb *verb options* *verb positional arguments* -- *arguments to be passed to the remote process*`

A double dash separates the options for gExec itself from the options to be passed to the executable being called - unlike programs such as docker, gExec's command-line parser cannot interpret the difference between the two automatically, and this double dash **must** be used. I would recommend starting from the examples and modifying to suit your use case. There may be holes in how I've utilised the command-line parser, but I would still recommend following the proper format rigorously.

Calling `gexec --help` will display help for global options and the available verbs. To get help for a specific verb, use e.g. `gexec server --help`

For the client, the address and protocol are taken from a connection string - this is [documented here](https://github.com/grpc/grpc/blob/master/doc/naming.md).  Note that for TCP, we don't specify the protocol, simple `host:port` as the connection string. This is the design of the underlying library. For the server, the protocol is specified, followed by the address, i.e., `unix path` or `tcp host:port`. The discrepancy between the two is currently due to limits in the underlying libraries - I prefer the connection string overall but  didn't find a way to replicate this for the server as can be done for the client without doing this manually.


## Known issues
- I've spent some time tidying and neatening the project, but it certainly could be tightened up in a number of areas. Some options and arguments might be better renamed; documentation could be improved, and errors could be used more thoroughly. Expect some inconsistencis as I didn't want more phantom projects hanging around my hard drive never seeing the light of day again. I'd be surprised if there aren't some bugs at this stage.

- stdout and stderr may not be captured "in order" - the terminal typically muxes these two technically distinct streams in a way that makes interactive processing basically seamless. When run with gExec, these are currently captured separately and written out to gExec's own file descriptors separately. Therefore, if a script returns output and error information from multiple commands interleaved with each other, this may well not happen when called with gExec. I don't believe there's a simple solution to this in general, AFAIK your chosen shell is typically using some optimised buffers to make sure everything is seen in a helpful manner, but I don't think there's any inherent underlying guarantee about ordering as the two are distinct. This would probably be addressed with the streaming mode of operation, so I didn't bother fixing it and don't need it for my use case for this.

- The aim was to write something small, yet tidy, and useful for others at large. However, after trying Dart initially, this is my first project to test-drive Go, and I didn't take to it. I am not averse to mainaining the project, but equally, don't necessarily want to expand it as I would prefer to write projects in another language. I'd call it a work in progress but don't necessarily plan to progress with it if I don't need to. Time will tell. Currently as of the first public commits, as above some more polishing and better error handling could probably be implemented, but the core functionality seems to work and I wanted this ticked off my list for the time being.

- This readme could also probably be expanded. But I want to go for a walk and have my dinner :)
