# Use alpine for musl, or e.g. just 1.18 for glibc.

#ARG SourceTag="1.18"
ARG SourceTag="1.18-alpine"

ARG SourceImage=golang:${SourceTag}

# We can't use from Scratch as the program is too complex, and a static build is not possible - a C library is needed for networking etc.
# busybox is a very small image providing glibc etc.; for their musl variant, this is statically built and so missing from the image.

#ARG FinalBase=busybox:glibc
ARG FinalBase=alpine


FROM ${SourceImage} as gexec_build


RUN go install 'gitlab.com/stellarpower/gexec@latest'


FROM ${FinalBase}

COPY --from=gexec_build /go/bin/gexec /gexec

CMD /gexec
