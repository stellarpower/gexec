// grpcClient.go implements the client side of the process

package main

import (
	"context"
	"log"
    "bufio"
	"time"
    "github.com/kr/pretty"   
	"google.golang.org/grpc"
)

const (
    DefaultDialTimeout = 15 * time.Second
)
// Todo: add command-line flag for this

// Options relating to the application, as client
type clientOptions struct {
    executable string
    args []string

    stdin string
    stdout bufio.Writer
    stderr bufio.Writer
    
}

// Options relating to the GRPC process only, as client
type gRPCClientOptions struct{
    addressWithProtocol  string
    dialTimeout          time.Duration
    execTimeout          *time.Duration  // How long the client will wait for the application to run on the remote host. 
                                         // Default to nil to wait indefinitely
}
    

func startGRPCClient(address string, opts clientOptions, gOpts gRPCClientOptions) (int32, error){
	// Set up a connection to the server.
    
    dialCtx, dialCancel := context.WithTimeout(context.Background(),  gOpts.dialTimeout)
	conn, err := grpc.DialContext(dialCtx, gOpts.addressWithProtocol, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
    defer conn.Close()
    defer dialCancel()
	
	client := NewGExecClient(conn)

	// Contact the server and print out its response.

    // As we are executing processes, this could take arbitrarily long.
    // Add a comand-line flag for a timeout in the future.
    
    // If not specified, never times out
    runCtx := context.Background()
    if gOpts.execTimeout != nil{
        // Else, times out after a fixed time.
        var runCancel context.CancelFunc
        runCtx, runCancel = context.WithTimeout(context.Background(), *gOpts.execTimeout)
        defer runCancel()   
    }
	
    
    var request OneshotRequest
    if (opts.executable == ""){
        request = OneshotRequest{
            Request: &OneshotRequest_Default{&OneshotRequest_DefaultExecutable {
                Args:   opts.args,
                Stdin:  opts.stdin,           
            }},
        }
    } else {
        request = OneshotRequest{
            Request: &OneshotRequest_Custom{&OneshotRequest_CustomExecutable {
                Executable: opts.executable,
                Args:       opts.args,
                Stdin:      opts.stdin,
            }},
        }
    }

	response, err := client.ExecOneshot(runCtx, &request)
	if err != nil {
		log.Fatalf("Could not call ExecOneshot: %v", err)
	}
    switch r := response.Response.(type) {
		case *OneshotResponse_Success:{
			//pretty.Println(r.Success)
            opts.stdout.WriteString(r.Success.Stdout)
            opts.stderr.WriteString(r.Success.Stderr)
            opts.stdout.Flush()
            opts.stderr.Flush()

            return r.Success.RetCode, nil

		}
		case *OneshotResponse_Failure:{
			pretty.Println(r.Failure)
		}
		case nil: { pretty.Println("Response is nil") }
		default:  { pretty.Println("Response is of unknown type") }
		}
    
	return 0, nil // FIXME
}
