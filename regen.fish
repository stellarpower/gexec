#!/usr/bin/fish

# Regenerates the protocol buffers using my setup.

/snap/bin/protoc  --plugin ~/go/pkg/mod/google.golang.org/protobuf@v1.25.0/ --experimental_allow_proto3_optional  --go_out=./ --go_opt=paths=source_relative --go-grpc_out=./ --go-grpc_opt=paths=source_relative -I./ ./gExec.proto

