/* 
 * gExec copyright 2021 Ben Southall
 *
 * Original sample code copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package main

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"io"
	"bufio" 
	
	"github.com/jessevdk/go-flags"
	"github.com/kr/pretty"
)

var globalExitStatus *int32

// NOTE: struct fields must also be capitalised to be externally visible
type clientOpts struct {
	Input      string               `short:"I" long:"input"       description:"Provide given text to stdin of the run process in one lump"`
	InputFile  flags.Filename       `short:"i" long:"input-file"  description:"Provide text read from given file to stdin of the run process in one lump"`
	OutputFile func(flags.Filename) `short:"o" long:"output-file" description:"Output stdout of run process to this file (in lieu of stdout of gExec)"`
	ErrorFile  func(flags.Filename) `short:"e" long:"error-file"  description:"Output stderr of run process to this file (in lieu of stderr of gExec)"`
	Executable      string       `short:"x" long:"executable"       description:"Custom executable to be run. If the server is locked and this does not match the default, an error will occur. If empty, the server and the sever does not have a default, an error will occur."`
	Positional struct {
		Address string      `positional-arg-name:"address" required:"true" description:"e.g. unix:///path/to/socket, host:port, ..." long-description:"See https://github.com/grpc/grpc/blob/master/doc/naming.md for dial formats"`
		ChildProcessArgs []string `positional-args:"yes"`
	} `positional-args:"yes"`

}

type serverOpts struct {
	Locked          bool        `short:"l" long:"locked"               description:"Only the provided executable may be invoked; Prevents arbitrary code execution by the client (recommended)"`
	Executable      string      `short:"x" long:"default-executable"   description:"Default executable to be run if none is provided by the client, or the only executable that may be run is locked mode is enabled."`

	Positional struct {
		Protocol    string      `positional-arg-name:"protocol"        description:"unix|tcp|... (see Go documentation for net.listen)"`
		Address     string      `positional-arg-name:"address"         description:"address for the above protocol, e.g. a unix filepath|port|... (see Go documentation for net.listen)"`
		DefaultArgs []string    `positional-args:"yes" optional:"yes" positional-arg-name:"default-args" description:"Default arguments, separated by a '--' to be passed to the executable; arguments provided by the client are *appended* to these."`
	} `positional-args:"yes"`
}

type globalOpts struct {  
	Stream     func(bool)       `long:"stream"      description:"Communicate in realtime with the executed process (currently unimplemented). Default is false (oneshot mode)."`
}

func addSub(name string, desc string, longDesc string, parser *flags.Parser, opts interface{}){
	cmd, e := parser.AddCommand(name, desc, longDesc, opts)
	if e != nil {
		panic(e)
	}
	cmd.SubcommandsOptional = false
}

///////////////////////////////////////

// Handlers called by the command-line parser
// As we have subcommands, we use these to perform the actual work.

func (this *clientOpts) Execute(excessPositional []string) error {
	// Manual option parsing
	switch {
		case (this.Input != "") && (this.InputFile != ""): {
			return errors.New("Only one of `input` and `--input-file` may be specified - the client will not take input from both the command line and a file")	
		}
		case (this.Input != ""): {
			// Take input to remote executable from the provided string
			inputReader = *bufio.NewReader(strings.NewReader(this.Input))
		}
		case (this.InputFile != ""): {
			// Take input to remote executable from a file
			file, err := os.Open(string(this.InputFile))
			if err == nil {
				inputReader = *bufio.NewReader(file)
			} else {
				panic(err)
			}
		}

		// No input string or file - try to read from STDIN
		default: {
			// Check if data has been piped in
			stat, _ := os.Stdin.Stat()
			if (stat.Mode() & os.ModeCharDevice) == 0 {
				// Data has been piped in, read from stdin
				inputReader = *bufio.NewReader(os.Stdin)

			}
			// Else, interactive use not supported whilst one-shot is the only mode
			// The default above will  just send an empty string.
			
		}
	}

	// Setup and start the client

	// Get all the input we want to provide as a string
	var readInput string
	if buffer, err := io.ReadAll(&inputReader); err == nil {
		readInput = string(buffer)
		//fmt.Println("input is", readInput)
	} else {
		panic(err)
	}
	
	// Options specifically for gExec
	opts := clientOptions{
		this.Executable,                   // Remote executable to call
		this.Positional.ChildProcessArgs,  // Args for the remote process
		readInput, 						   // Stdin for it
		outputWriter,                      // Means to output stdout
		errorWriter,                       // Means to output stderr
	}

	// Options for how GRPC is handled
	gRPCOpts := gRPCClientOptions{
		addressWithProtocol: this.Positional.Address,   // e.g. unix:///tmp/sock , tcp://192.168.0.100:9000
		dialTimeout:         DefaultDialTimeout,        // Time to wait before giving up establishing connection
	}

	retCode, error := startGRPCClient(this.Positional.Address, opts, gRPCOpts)
	globalExitStatus = &retCode
	return error
}

func (this *serverOpts) Execute(excessPositional []string) error {
	//pretty.Print("Server called:\n\n", this, "\n\n excess:", excessPositional, "\n")
	if (this.Locked) && (this.Executable == ""){
		return errors.New("If locked mode is specified, executable must be provided")
	}
	
	serviceHandler := LibServer{
		this.Executable,
		this.Positional.DefaultArgs,
		this.Locked,
	}

	gRPCOptions := GRPCServerOptions{
		this.Positional.Protocol,
		this.Positional.Address,
	}

	return startOneshotServer(gRPCOptions, &serviceHandler)
	return nil
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

// defaults
var inputReader  bufio.Reader = *bufio.NewReader(strings.NewReader(""))
var outputWriter bufio.Writer = *bufio.NewWriter(os.Stdout)
var errorWriter  bufio.Writer = *bufio.NewWriter(os.Stderr)

//Callbacks for the associated flags. Write out to file rather than stdout/stderr

func outputFile(path flags.Filename){
	file, err := os.Create(string(path))
	if err == nil {
		outputWriter = *bufio.NewWriter(file)
	} else {
		panic(err)
	}
}

func errorFile(path flags.Filename){
	file, err := os.Create(string(path))
	if err == nil {
		errorWriter = *bufio.NewWriter(file)
	} else {
		panic(err)
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////

func main() {

	var client clientOpts
	var server serverOpts
	var global globalOpts

	client.OutputFile = outputFile
	client.ErrorFile  = errorFile

	global.Stream = func(bool){
		panic(errors.New("Streaming functionality is not currently implemented. Aborting."))
	}

	parser := flags.NewParser(&global, flags.HelpFlag | flags.PassDoubleDash | flags.PrintErrors)
	parser.SubcommandsOptional = false
	
	addSub("client", "Start in client mode", "Connect to server and run remote binary or script", parser, &client)
	addSub("server", "Start in server mode", "Begin server to service requests and run executable", parser, &server)


	remaining, err := parser.Parse()
	if (err != nil) && (err != flags.ErrHelp) {
		//fmt.Println(err)
		switch /*t :=*/ err.(type){
			case *flags.Error:
				pretty.Println("*flags.Error")
				//case errors.Something:			
			default:
				pretty.Println("gExec internal error:")
				pretty.Println(err)
				panic(err)	
		}
	}

	if len(remaining) != 0{
		panic(fmt.Sprint("Command line parser - unhandled arguments remain: ",  remaining))
	}

	if globalExitStatus != nil{
		defer os.Exit(int(*globalExitStatus))
	}
	
}
	
