module gitlab.com/stellarpower/gexec

go 1.16

// Use this reference: https://golang.org/doc/code

require (
	github.com/golang/protobuf v1.5.2
	github.com/jessevdk/go-flags v1.5.0
	github.com/kr/pretty v0.3.0
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	gitlab.com/StellarpowerGroupedProjects/tidbits/go v0.0.0-20240319161019-8993a8185e99 // indirect
	gitlab.com/stellarpower/go-runprocess v0.0.0-20210807211816-94fc9a19658e
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e
	google.golang.org/genproto v0.0.0-20210809142519-0135a39c2737 // indirect
	google.golang.org/grpc v1.39.1
	google.golang.org/protobuf v1.27.1
)
