// grpcServer.go implements the GRPC half of the server-side of the process,
// libServer.go manages actually calling out to the executable and running it

package main

import (
	"context"
	"log"
	"net"
	"sync"
	
	"github.com/kr/pretty"
	"google.golang.org/grpc"

	"gitlab.com/StellarpowerGroupedProjects/tidbits/go"

)




// Implements the functionality exposed via the protocol buffer interface.
type applicationServer struct {
    serviceHandler *LibServer
	UnimplementedGExecServer
}

func (this *applicationServer) ExecOneshot(ctx context.Context, request *OneshotRequest) (*OneshotResponse, error) {
	log.Println(pretty.Sprint("Handling request: "))//, request))
	// Call out to libServer
    response, err := this.serviceHandler.execOneshot(request)
	// return the response structure
	return response, err
}

type GRPCServerOptions struct{
	bindProtocol string  // unix, tcp,     ...
	bindAddress  string  // path, IP:port, ... 
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Start the server in oneshot mode
func startOneshotServer(options GRPCServerOptions, serviceHandler *LibServer) error {

	lis, err := net.Listen(options.bindProtocol, options.bindAddress)
	if err != nil {
		log.Fatalf("net.listen failed: %v", err) //Return error here
	}
	
	gRPCServer := grpc.NewServer()

	// gRPCServer services the GRPC requests
	// serviceHandler actually performs the logic of calling out processes
	// applicationServer ideally only implements the minimal needed to tie these two together

	RegisterGExecServer(gRPCServer, &applicationServer{
        serviceHandler: serviceHandler,
    })

	// https://stackoverflow.com/questions/16681944/how-to-reliably-unlink-a-unix-domain-socket-in-go-programming-language
	// Handle common process-killing signals so we can gracefully shut down and the unix socket will be deleted, amongst other benefits:
	var waiter sync.WaitGroup
	waiter.Add(1)
	go func(){
		// Worker thread that starts the server, because the Serve call will block
		log.Printf("Server starting on %v", lis.Addr())
		if err := gRPCServer.Serve(lis); err != nil {
			log.Fatalf("Failed to start GRPC Server - Serve threw: %v", err) // Return error here
		}
		log.Printf("Server listening on %v stopped", lis.Addr())
		waiter.Done()
	}()

	// Unix sockets should be cleaned up if we have a graceful shutdown
	// We add a goroutine to the waitgroup that will handle the signals for us
	tidbits.WaitGroupAddTerminate(&waiter, func() error{
		// This callback is called once the signal has been caught.
		// Simply sk the server to stop. The above goroutine will then run to completion
		gRPCServer.GracefulStop()
		return nil
	})
	// Wait for the server and the singal handler goroutines.
	waiter.Wait()
	
	return nil
}
