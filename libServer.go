// libServer.go contains the logic for running the process and handling its input/output
// It also checks permissions on the desired file and checks that when the server is locked
// to a specific executable, the client is unable to execute an arbitrary process

package main

import (
	"path"

    "gitlab.com/stellarpower/go-runprocess"
	"gitlab.com/StellarpowerGroupedProjects/tidbits/go"

)

// Fixme - how to put in the protocol buffer? We need to include any positive OS error.
type ApplicationError int
const (
    UnknownError ApplicationError   = -iota 
    LockedPermission                = -iota 
    //ExecutableMismatch              = -iota 
)
const (
	UnknownErrorMessage      = "An unknown error has ocurred"
	LockedPermissionMessage  = "(Arbitrary) executable provided does not match the allowed files for execution by the server"
	//ExecutionMismatchMessage = "The executable provided does not match the executable allowed by the server"
)

type LibServer struct {
	executable    string
    defaultArgs   []string
    lockedMode    bool
}

/*func NewLibServer() LibServer {

} //File{fd, name, nil, 0}*/

/*
 * ## Message format notes
 *
 * ### The protocol buffer compiler generates the following:
 *
 * package: types are prefixed by the package specified in the proto definition
 * 
 * Fields: Generated fields are always UpperCamelCase. The protocol file is converted from snake case if present.
 * Optional fields simply become their pointer types - check if they're nil to see if present
 *  - Getter GetFoox will provide the default value if it is not present (this is zero-value if ont provided)
 * Repeated Fields: A pointer to a slice is generated: Foo []*Bar
 * OneOf: a single field that implements an interface is generated. The possible options are also types that implement this interface
 *  - switch on the type of the field to access the generated struct
 *  - then use the generated method to acces the data
 * Nested types: Foo, Foo_Bar (not Foo.Bar)
 * 
 */

// This should not be called with an arbitrary executable from the client - 
// `executable` should already have been sanitised
// Checks that this file can be executed by this user (process), and returns more information
// if not.
// Wraps around the call to helper function, appending default arguments the server has specified and 
// filling in the structure we use for returning everything back.
func (this *LibServer) execHelper(executable string, args []string, stdin string) (*OneshotResponse, error) {
	if err := tidbits.CheckFileExecutable(executable, true); err != nil{
		return nil, err
	}
	returnCode, stdout, stderr  := runprocess.RunProcessSynchronous(executable, append(this.defaultArgs, args...), stdin)
	
	// TODO : Check here for errors properly.

	response := &OneshotResponse{
		Response: &OneshotResponse_Success{&OneshotResponse_Completed {
			RetCode: int32(returnCode),
			Stdout:  stdout,
			Stderr:  stderr,
		}},
	}

	return response, nil
}


// Helper that simply fills in the structure GRPC uses.
func returnError(code ApplicationError, message string) *OneshotResponse {
	return &OneshotResponse{
		Response: &OneshotResponse_Failure{&OneshotResponse_Error {
			Code:     int32(code),
			Message:  message,
		}},
	}
}

// Handle a oneshot request
// This requires all stdin be provided to it, and returns stdout and stderr as strings
// The currently unimplemented alternatrive would be to use GRPC's streaming concepts
// To have an interactive flow of packets of input and output between client and server.

func (this *LibServer) execOneshot(request *OneshotRequest) (*OneshotResponse, error) {
	// The server will not execute arbitrary files and only handle requests for the given executable.
	// This is recommended, to prevent execution of arbitrary code.
	// Example: the server is locked to the default binary of /bin/ls
	if this.lockedMode{
		switch r := request.Request.(type) {
			// If the client didn't specify an executable, we know what we're calling
			// Example: the client did not specify anything
			case *OneshotRequest_Default:{
				r__ := r.Default
				return this.execHelper(this.executable, append(this.defaultArgs, r__.Args...), r__.Stdin)
			}
			// But if they did, if the two match then we can still service the request.
			case *OneshotRequest_Custom:{
				r__ := r.Custom
				switch e__ := r__.Executable; {
					// Example: client provided /bin/ls
					case e__ == this.executable:
						return this.execHelper(this.executable, append(this.defaultArgs, r__.Args...), r__.Stdin)
					// Example: client provided ls
					case e__ == path.Base(this.executable):
						return this.execHelper(this.executable, append(this.defaultArgs, r__.Args...), r__.Stdin)
					// Example: client provided /usr/bin/ls, /bin/sh, chuckle-brothers, ....
					default:
						// These two are the same error in essence - which makes more sense?
						//return &returnError(ExecutableMismatch, ExecutableMismatchMessage), nil
						return returnError(LockedPermission, LockedPermissionMessage), nil
				}

			}
			case nil: { return nil, nil } // Generate "unknown error"
			default:  { return nil, nil  }
		}
	} else {
		// The client may specify the path to any file present on this machine that is executable to
		// this process
		// This is not generally recommended, as it opens an obvious security hole.
		switch r := request.Request.(type) {
			// If we provided a default, and the client didn't specify any, call the default
			case *OneshotRequest_Default:{
				r__ := r.Default
				return this.execHelper(this.executable, append(this.defaultArgs, r__.Args...), r__.Stdin)
			}
			// If the client did specify somehting, call that
			case *OneshotRequest_Custom:{
				r__ := r.Custom
				return this.execHelper(r__.Executable, append(this.defaultArgs, r__.Args...), r__.Stdin)
			}
			case nil: { return nil, nil } // Generate "unknown error"
			default:  { return nil, nil  }
		}
	}
}

